package dalapo.factech;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiLanguage;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.monster.EntityEnderman;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.potion.Potion;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.client.event.GuiScreenEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.living.EnderTeleportEvent;
import net.minecraftforge.event.entity.living.LivingDropsEvent;
import net.minecraftforge.event.entity.living.LivingEntityUseItemEvent;
import net.minecraftforge.event.entity.living.LivingSpawnEvent;
import net.minecraftforge.event.entity.player.ArrowLooseEvent;
import net.minecraftforge.event.entity.player.AttackEntityEvent;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.EntityEntry;
import net.minecraftforge.fml.common.registry.EntityRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.oredict.OreDictionary.OreRegisterEvent;

import java.io.UnsupportedEncodingException;

import dalapo.factech.auxiliary.MachineRecipes;
import dalapo.factech.auxiliary.PotionLockdown;
import dalapo.factech.block.BlockBase;
import dalapo.factech.block.IBlockSpecialItem;
import dalapo.factech.config.FacTechConfigManager;
import dalapo.factech.entity.EntityHoverScooter;
import dalapo.factech.entity.EntityPressureGunShot;
import dalapo.factech.helper.FacBlockHelper;
import dalapo.factech.helper.FacEntityHelper;
import dalapo.factech.helper.FacMathHelper;
import dalapo.factech.helper.Logger;
import dalapo.factech.init.BlockRegistry;
import dalapo.factech.init.DictRegistry;
import dalapo.factech.init.ItemRegistry;
import dalapo.factech.init.RecipeRegistry;
import dalapo.factech.item.block.ItemBlockSubtypes;
import dalapo.factech.reference.NameList;
import dalapo.factech.tileentity.TileEntityMachine;
import dalapo.factech.tileentity.automation.TileEntityTank;
import dalapo.factech.tileentity.specialized.TileEntityDisassembler;

@Mod.EventBusSubscriber
public class FacTechEventManager {
	private FacTechEventManager() {}
	
	public static FacTechEventManager instance = new FacTechEventManager();
	
	@SubscribeEvent
	public void registerBlocks(RegistryEvent.Register<Block> event)
	{
		Logger.info("Entered registerBlocks");
		BlockRegistry.init();
		for (Block b : BlockRegistry.blocks)
		{
			event.getRegistry().register(b);
		}
	}
	
	@SubscribeEvent
	public void registerEntities(RegistryEvent.Register<EntityEntry> event)
	{
		event.getRegistry().register(new EntityEntry(EntityPressureGunShot.class, "PressureGunShot").setRegistryName("PressureGunShot"));
		event.getRegistry().register(new EntityEntry(EntityHoverScooter.class, "hoverscooter").setRegistryName("hoverscooter"));
		int id = 0;
		EntityRegistry.registerModEntity(new ResourceLocation(NameList.MODID, "pressureshot"), EntityPressureGunShot.class, "pressureshot", id++, FactoryTech.instance, 64, 3, true);
		EntityRegistry.registerModEntity(new ResourceLocation(NameList.MODID, "hoverscooter"), EntityHoverScooter.class, "hoverscooter", id++, FactoryTech.instance, 64, 300, true);
	}
	
	@SubscribeEvent
	public void registerItems(RegistryEvent.Register<Item> event)
	{
		ItemRegistry.init();
		for (BlockBase b : BlockRegistry.blocks)
		{
			if (b instanceof IBlockSpecialItem)
			{
				event.getRegistry().register(new ItemBlockSubtypes(b).setRegistryName(b.getRegistryName()));
			}
			else
			{
				event.getRegistry().register(new ItemBlock(b).setRegistryName(b.getRegistryName()));
			}
		}
		for (Item i : ItemRegistry.items)
		{
			event.getRegistry().register(i);
		}
	}
	
//	@SubscribeEvent(priority=EventPriority.LOWEST)
//	public void registerExtractor(OreRegisterEvent event)
//	{
//		FacTechConfigManager.initDeepDrillCustomOres();
//	}
	
	@SubscribeEvent
	public void registerPotions(RegistryEvent.Register<Potion> event)
	{
		event.getRegistry().register(PotionLockdown.INSTANCE);
	}
	
	@SubscribeEvent
	public void registerRecipes(RegistryEvent.Register<IRecipe> event)
	{
		DictRegistry.registerOreDictEntries();
		RecipeRegistry.init();
	}
	
	@SubscribeEvent
	public void cancelDisassemblyDrops(LivingDropsEvent evt)
	{
		if (evt.getSource().damageType.equals("machine") && MachineRecipes.DISASSEMBLER.containsKey(evt.getEntityLiving().getClass()))
		{
			evt.setCanceled(true);
		}
	}
	
	@SubscribeEvent
	public void blockTeleport(EnderTeleportEvent evt)
	{
		if (evt.getEntityLiving() instanceof EntityEnderman)
		{
			BlockPos enderPos = evt.getEntity().getPosition();
			if (evt.getEntityLiving().world.getTileEntity(FacMathHelper.withOffset(enderPos, EnumFacing.DOWN)) instanceof TileEntityDisassembler ||
					evt.getEntityLiving().getActivePotionEffect(PotionLockdown.INSTANCE) != null)
			{
				evt.setCanceled(true);
			}
		}
	}
	
	@SubscribeEvent
	public void noShootingForYou(LivingEntityUseItemEvent.Start e)
	{
		if (!(e.getEntityLiving() instanceof EntityPlayer) && e.getEntityLiving().getActivePotionEffect(PotionLockdown.INSTANCE) != null && e.getItem().getItem() == Items.BOW)
		{
			e.setCanceled(true);
		}
	}
	
	@SubscribeEvent
	public void syncTankFill(FluidEvent e)
	{
		TileEntity te = e.getWorld().getTileEntity(e.getPos());
		// Don't mess with other mods' stuff
		Block b = e.getWorld().getBlockState(e.getPos()).getBlock();
		if (BlockRegistry.blocks.contains(b))
		{
			FacBlockHelper.updateBlock(e.getWorld(), e.getPos());
			if (te instanceof TileEntityMachine)
			{
				((TileEntityMachine)te).getHasWork();
			}
		}
	}
	
	@SubscribeEvent
	@SideOnly(Side.CLIENT)
	public void updateLanguage(GuiScreenEvent.ActionPerformedEvent e)
	{
		if (e.getGui() instanceof GuiLanguage)
		{
			((ClientProxy)FactoryTech.proxy).language = Minecraft.getMinecraft().getLanguageManager().getCurrentLanguage().getLanguageCode();
			try {
			((ClientProxy)FactoryTech.proxy).initHandbookPages();
			}
			catch (UnsupportedEncodingException ex)
			{
				Logger.fatal("Nope, that's not the right charset!");
				throw new RuntimeException();
			}
		}
	}
	
	@SubscribeEvent
	public void giveHandbookToPlayer(EntityJoinWorldEvent e)
	{
		if (FacTechConfigManager.automaticallyGiveHandbook && !e.getEntity().getEntityWorld().isRemote && e.getEntity() instanceof EntityPlayer)
		{
			EntityPlayer ep = (EntityPlayer)e.getEntity();
			NBTTagCompound nbt = FacEntityHelper.getPersistentTag(ep);
			if (ItemRegistry.handbook != null && !nbt.getBoolean("givenHandbook"))
			{
				ItemStack book = new ItemStack(ItemRegistry.handbook);
				NBTTagCompound bookInfo = new NBTTagCompound();
				bookInfo.setInteger("section", 0);
				bookInfo.setInteger("page", 0);
				bookInfo.setInteger("entry", 0);
				book.setTagCompound(bookInfo);
				if (!ep.addItemStackToInventory(book))
				{
					EntityItem ei = new EntityItem(ep.getEntityWorld(), ep.posX, ep.posY, ep.posZ, book);
					ep.getEntityWorld().spawnEntity(ei);
				}
				nbt.setBoolean("givenHandbook", true);
			}
		}
	}
}
