package dalapo.factech.item.block;

import java.util.List;

import dalapo.factech.block.BlockBase;
import dalapo.factech.block.IBlockSpecialItem;
import dalapo.factech.reference.NameList;
import net.minecraft.block.Block;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemBlockSubtypes extends ItemBlock
{

	public ItemBlockSubtypes(BlockBase block)
	{
		super(block);
		if (!(block instanceof IBlockSpecialItem)) throw new IllegalArgumentException();
		this.setMaxDamage(0);
		this.setHasSubtypes(true);
	}
	
	public int getMetadata(int dmg)
	{
		return dmg;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(ItemStack is, World world, List<String> list, ITooltipFlag flags)
	{
		super.addInformation(is, world, list, flags);
	}
	
	@Override
	public String getUnlocalizedName(ItemStack stack)
	{
		return ((IBlockSpecialItem)block).getName(stack) + ":" + stack.getItemDamage();
	}
}