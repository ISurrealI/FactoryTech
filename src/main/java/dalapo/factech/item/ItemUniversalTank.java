package dalapo.factech.item;

import javax.annotation.Nullable;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.FluidTankProperties;
import net.minecraftforge.fluids.capability.IFluidHandlerItem;
import net.minecraftforge.fluids.capability.IFluidTankProperties;

public class ItemUniversalTank extends ItemBase
{
	public ItemUniversalTank(String name)
	{
		super("universaltank");
	}
	
	@Override
	public ICapabilityProvider initCapabilities(ItemStack is, NBTTagCompound nbt)
	{
		if (!is.hasTagCompound() || nbt == null)
		{
			NBTTagCompound tag = new NBTTagCompound();
			tag.setString("fluid", "<empty>");
			is.setTagCompound(tag);
		}
		else
		{
			is.setTagCompound(nbt); // Should probably look into how other mods implement this
		}
		return new UniversalTankCapability(is);
	}
	
	private static class UniversalTankCapability implements IFluidHandlerItem, ICapabilityProvider
	{
		@Nullable Fluid fluid;
		ItemStack container;
		
		UniversalTankCapability(ItemStack is)
		{
			this(is, null);
		}
		
		UniversalTankCapability(ItemStack is, NBTTagCompound nbt)
		{
			if (nbt == null) fluid = null;
			else fluid = FluidRegistry.getFluid(nbt.getString("fluid"));
			container = is;
		}
		
		@Override
		public boolean hasCapability(Capability<?> capability, EnumFacing facing) {
			return capability == CapabilityFluidHandler.FLUID_HANDLER_ITEM_CAPABILITY;
		}

		@Override
		public <T> T getCapability(Capability<T> capability, EnumFacing facing)
		{
			if (capability == CapabilityFluidHandler.FLUID_HANDLER_ITEM_CAPABILITY) return CapabilityFluidHandler.FLUID_HANDLER_ITEM_CAPABILITY.cast(this);
			else return null;
		}

		@Override
		public IFluidTankProperties[] getTankProperties()
		{
			return new FluidTankProperties[] {new FluidTankProperties(new FluidStack(fluid, Fluid.BUCKET_VOLUME), Fluid.BUCKET_VOLUME)};
		}

		@Override
		public int fill(FluidStack resource, boolean doFill) {
			if (fluid != null || resource.amount != 1000) return 0; // No partial fills, only fill if it's empty
			if (doFill)
			{
				fluid = resource.getFluid();
				return 1000;
			}
			else return 1000;
		}

		@Override
		public FluidStack drain(FluidStack resource, boolean doDrain) {
			if (fluid == null || resource.amount < 1000 || resource.getFluid() != fluid) return null;
			if (doDrain) fluid = null;
			return new FluidStack(resource.getFluid(), 1000);
		}

		@Override
		public FluidStack drain(int maxDrain, boolean doDrain) {
			if (maxDrain < 1000 || fluid == null) return null;
			FluidStack toReturn = new FluidStack(fluid, 1000);
			if (doDrain) fluid = null;
			return toReturn;
		}

		@Override
		public ItemStack getContainer()
		{
			return container;
		}
		
	}
}