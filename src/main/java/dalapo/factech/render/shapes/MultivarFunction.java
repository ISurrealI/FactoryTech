package dalapo.factech.render.shapes;

import net.minecraft.util.math.Vec2f;
import net.minecraft.util.math.Vec3d;

@FunctionalInterface
public interface MultivarFunction<T>
{
	public T apply(Vec2f vars, float... constants);
}