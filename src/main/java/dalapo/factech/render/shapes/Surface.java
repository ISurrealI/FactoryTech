package dalapo.factech.render.shapes;

import java.util.function.BiFunction;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.math.Vec2f;
import net.minecraft.util.math.Vec3d;

public class Surface
{
	private BufferBuilder pos(BufferBuilder buffer, Vec3d vector)
	{
		buffer.pos(vector.x, vector.y, vector.z);
		return buffer;
	}
	
	public void render(Vec3d origin, float maxU, float maxV, float du, float dv, MultivarFunction<Vec3d> r, float...constants)
	{
		GlStateManager.pushMatrix();
		GlStateManager.translate(origin.x, origin.y, origin.z);
		Tessellator v5 = Tessellator.getInstance();
		BufferBuilder buffer = v5.getBuffer();
		buffer.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION);
		for (float u=0; u<maxU; u+=du)
		{
			for (float v=0; v<maxV; v+=dv)
			{
				pos(buffer, r.apply(new Vec2f(u, v), constants)).endVertex();
				pos(buffer, r.apply(new Vec2f(u, v+dv), constants)).endVertex();
				pos(buffer, r.apply(new Vec2f(u+du, v+dv), constants)).endVertex();
				pos(buffer, r.apply(new Vec2f(u+du, v), constants)).endVertex();
			}
		}
		v5.draw();
		GlStateManager.popMatrix();
	}
}