package dalapo.factech.render.tesr;

import org.lwjgl.opengl.GL11;

import dalapo.factech.block.BlockDirectional;
import dalapo.factech.tileentity.automation.TileEntityConveyor;
import dalapo.factech.tileentity.automation.TileEntityTrapdoorConveyor;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BlockRendererDispatcher;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.world.World;

public class TesrTrapdoorConveyor extends TesrConveyor
{
	@Override
	public void renderRollers(TileEntityConveyor te)
	{
		World world = te.getWorld();
		if (world.isBlockPowered(te.getPos())) super.renderRollers(te); // Conditional overriding? Conditional overriding.
		else
		{
			for (double d=0; d<1; d+=0.125)
			{
				GlStateManager.pushMatrix();
				Tessellator v5 = Tessellator.getInstance();
				BufferBuilder builder = v5.getBuffer();
				IBlockState state = te.getBlockType().getDefaultState().withProperty(BlockDirectional.PART_ID, 1);
				BlockRendererDispatcher dispatcher = Minecraft.getMinecraft().getBlockRendererDispatcher();
				IBakedModel model = dispatcher.getModelForState(state);
				builder.begin(GL11.GL_QUADS, DefaultVertexFormats.BLOCK);
				GlStateManager.translate(-0.4375, 0.03125, d-0.46875);
				GlStateManager.scale(0.875, 1, 1);
				long angle = System.currentTimeMillis() / 8 % 360;
				if (!Minecraft.getMinecraft().isGamePaused()) GlStateManager.rotate(angle, -1, 0, 0);
				GlStateManager.translate(-te.getPos().getX(), -te.getPos().getY()-0.03125, -te.getPos().getZ()-0.03125);
				dispatcher.getBlockModelRenderer().renderModel(world, model, state, te.getPos(), builder, false);	
				v5.draw();
				GlStateManager.popMatrix();
				if (d == 0.125) d = 0.625; // Don't render the middle rollers; show gap for items to fall thru
			}
		}
	}
}