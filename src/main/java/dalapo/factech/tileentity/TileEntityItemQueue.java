package dalapo.factech.tileentity;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

import dalapo.factech.auxiliary.QueuedItem;
import dalapo.factech.helper.FacBlockHelper;
import dalapo.factech.helper.Logger;
import dalapo.factech.init.BlockRegistry;
import dalapo.factech.reference.StateList;
import dalapo.factech.render.tesr.TesrElevator.TESRELEV;
import dalapo.factech.tileentity.automation.TileEntityElevator;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public abstract class TileEntityItemQueue extends TileEntityBase implements ITickable
{
	protected static boolean canItemsBePushed = true;
	
	private static final int DELAY = 5;
	private int ticks = 0;
	
	protected List<QueuedItem> stacks = new ArrayList<QueuedItem>();
	protected LinkedList<ItemStack> scheduled = new LinkedList<ItemStack>();
	protected BlockPos targetPos;
	private ItemStack legacy = ItemStack.EMPTY;
	
	public abstract BlockPos getTarget();
	protected abstract void ejectItem(ItemStack toEject);
	
	public TileEntityItemQueue(String name)
	{
		super(name);
//		for (int i=0; i<getCapacity(); i++)
//		{
//			stacks.add(new QueuedItem(ItemStack.EMPTY, this));
//		}
	}
	
	public boolean allowPush()
	{
		return canItemsBePushed;
	}
	
	@Override
	public void onLoad()
	{
		super.onLoad();
		FacBlockHelper.updateBlock(world, pos);
		targetPos = getTarget();
	}
	
	public List<QueuedItem> getStacks(TESRELEV auth)
	{
		return stacks;
	}
	
	public ItemStack yank(int index)
	{
		QueuedItem qi = null;
		for (int i=0; i<stacks.size(); i++)
		{
			if (stacks.get(i).getTime() == index)
			{
				qi = stacks.remove(i);
			}
		}
		FacBlockHelper.updateBlock(world, pos);
		return qi == null ? ItemStack.EMPTY : qi.getItem();
	}
	
	public void set(int index, ItemStack is)
	{
		boolean flag = false;
		for (int i=0; i<stacks.size(); i++)
		{
			if (stacks.get(i).getTime() == index)
			{
				stacks.set(i, new QueuedItem(is, this, index));
				flag = true;
				break;
			}
		}
		if (!flag)stacks.add(new QueuedItem(is, this, index));
		FacBlockHelper.updateBlock(world, pos);
	}
	
	public ItemStack peek(int index)
	{
		for (QueuedItem qi : stacks)
		{
			if (qi.getTime() == index) return qi.getItem();
		}
		return ItemStack.EMPTY;
	}
	
	@SideOnly(Side.CLIENT)
	public ItemStack getLegacy()
	{
		return legacy;
	}
	
	public void scheduleItemStack(ItemStack itemstack)
	{
		if (!itemstack.isEmpty()) scheduled.add(itemstack);
//		FacBlockHelper.updateBlock(world, pos); // Ugly, sure, but necessary for client/server syncing
	}
	
	private void scheduleItemStackWithPriority(ItemStack toEject)
	{
		scheduled.addFirst(toEject);
//		FacBlockHelper.updateBlock(world, pos);
	}
	
	@Override
	public void update()
	{
		legacy = ItemStack.EMPTY;
		if (--ticks<= 0 && !scheduled.isEmpty())
		{
			stacks.add(new QueuedItem(scheduled.remove(), this));
			ticks = DELAY;
			FacBlockHelper.updateBlock(world, pos);
		}
		
		for (int i=stacks.size()-1; i>=0; i--)
		{
			QueuedItem qi = stacks.get(i); // *angry klaxon noises*
			if (qi.tick())
			{
				if (!world.isRemote)
				{
					ItemStack toEject = qi.getItem();
					TileEntity te = world.getTileEntity(targetPos);
					boolean dealtWith = false;
					if (te instanceof TileEntityItemQueue)
					{
						((TileEntityItemQueue)te).scheduleItemStackWithPriority(toEject);
						dealtWith = true;
					}
					else if (world.getBlockState(targetPos).getBlock() == BlockRegistry.hatch)
					{
						toEject = BlockRegistry.hatch.insertItem(world, targetPos, toEject);
						if (toEject.isEmpty()) dealtWith = true;
					}
					if (!world.isRemote && !dealtWith)
					{
						ejectItem(toEject);
					}
				}
				legacy = qi.getItem();
				stacks.remove(i);
			}
		}
		/*
		if (scheduled.isEmpty()) stacks.addLast(new QueuedItem(ItemStack.EMPTY, this));
		else stacks.addLast(new QueuedItem(scheduled.remove(), this));
		ItemStack toEject = stacks.remove().getItem();
		if (!toEject.isEmpty())
		{
			TileEntity te = world.getTileEntity(targetPos);
			boolean dealtWith = false;
			if (te instanceof TileEntityItemQueue)
			{
				((TileEntityItemQueue)te).scheduleItemStack(toEject);
				dealtWith = true;
			}
			else if (world.getBlockState(targetPos).getBlock() == BlockRegistry.hatch)
			{
				toEject = BlockRegistry.hatch.insertItem(world, targetPos, toEject);
			}
			if (!world.isRemote && !dealtWith)
			{
				ejectItem(toEject);
			}
		}
		if (world.isRemote) legacy = toEject; // .copy()?
		*/
	}
	

	
	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound nbt)
	{
		super.writeToNBT(nbt);
		nbt.setInteger("ticks", ticks);
		nbt.setLong("pos", targetPos.toLong());
		NBTTagList list = new NBTTagList();
		for (QueuedItem qi : stacks)
		{
			list.appendTag(qi.serializeNBT());
		}
		nbt.setTag("items", list);
		return nbt;
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);
		targetPos = BlockPos.fromLong(nbt.getLong("pos"));
		ticks = nbt.getInteger("ticks");
		stacks.clear();
		NBTTagList list = nbt.getTagList("items", 10);
		for (NBTBase tag : list)
		{
			if (!(tag instanceof NBTTagCompound))
			{
				throw new RuntimeException("Somehow a non-itemstack has been stored in this list. Somebody should probably be fired over this.");
			}
			stacks.add(new QueuedItem((NBTTagCompound)tag, this));
		}
	}
	
	@Override
	public void invalidate()
	{
		for (QueuedItem qi : stacks)
		{
			if (!world.isRemote && !qi.getItem().isEmpty()) world.spawnEntity(new EntityItem(world, pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5, qi.getItem()));
		}
		super.invalidate();
	}
	
	public int getCapacity()
	{
		return 20;
	}
}
