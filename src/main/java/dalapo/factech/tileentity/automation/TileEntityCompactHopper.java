package dalapo.factech.tileentity.automation;

import dalapo.factech.helper.FacMathHelper;
import dalapo.factech.helper.FacTileHelper;
import dalapo.factech.reference.StateList;
import dalapo.factech.tileentity.TileEntityBasicInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;

public class TileEntityCompactHopper extends TileEntityBasicInventory implements ITickable
{
	private int ticks = 0;
	public TileEntityCompactHopper()
	{
		super("compacthopper", 1);
	}

	@Override
	public void update()
	{
		if (ticks++ >= 5 && !getStackInSlot(0).isEmpty() && world.isBlockIndirectlyGettingPowered(pos) == 0)
		{
			ticks = 0;
			ItemStack is = getStackInSlot(0).copy();
			is.setCount(1);
			EnumFacing dir = world.getBlockState(pos).getValue(StateList.directions);
			if (FacTileHelper.tryInsertItem(world.getTileEntity(FacMathHelper.withOffset(pos, dir)), is, dir.getOpposite().getIndex(), false).isEmpty()) decrStackSize(0, 1);
		}
	}
}