package dalapo.factech.tileentity.automation;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import dalapo.factech.helper.FacBlockHelper;
import dalapo.factech.helper.FacMathHelper;
import dalapo.factech.helper.Logger;
import dalapo.factech.init.BlockRegistry;
import dalapo.factech.reference.StateList;
import dalapo.factech.tileentity.IMagnifyingGlassInfo;
import dalapo.factech.tileentity.TileEntityBase;
import dalapo.factech.tileentity.TileEntityMachine;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.BlockPos;

public class TileEntityPartSensor extends TileEntityBase implements ITickable, IMagnifyingGlassInfo
{
	public TileEntityPartSensor()
	{
		super("partsensor");
	}

	private boolean hasFullyLoaded = false;
	private TileEntityPartSensor behind;
	private TileEntityMachine watchingMachine;
	private int watchingSlot = 0;
	private int output = 0;
	
	private void init(int slot, @Nullable TileEntityMachine machine)
	{
		watchingMachine = machine;
		watchingSlot = slot;
		TileEntity te = world.getTileEntity(FacMathHelper.withOffset(pos, direction.getOpposite()));
		if (te instanceof TileEntityPartSensor && ((TileEntityPartSensor) te).direction.equals(this.direction))
		{
			behind = (TileEntityPartSensor)te;
			behind.init(slot+1, machine);
		}
	}
	
	public void onLoad()
	{
		setDirection(world.getBlockState(pos).getValue(StateList.directions));
	}

	public void onNeighbourChange(BlockPos neighbourPos)
	{
		super.onNeighbourChange(neighbourPos);
	}
	
	private TileEntityMachine getMachine()
	{
		return watchingMachine;
	}
	
	@Override
	public void update() // Cannot do this in onLoad due to chunkloading restrictions. Wish there could be multiple phases of loading TEs
	{
		TileEntity te = world.getTileEntity(FacMathHelper.withOffset(pos, direction));
		if (te == null && watchingMachine != null)
		{
			init(0, null);
		}
		else if (te instanceof TileEntityPartSensor && ((TileEntityPartSensor)te).watchingMachine != null)
		{
			int stage = ((TileEntityPartSensor)te).watchingSlot + 1;
			init(stage, ((TileEntityPartSensor)te).watchingMachine);
		}
		else if (te instanceof TileEntityMachine && ((TileEntityMachine)te != watchingMachine))
		{
			init(0, (TileEntityMachine)te);
		}
		int newOutput = getMachine() == null || getMachine().hasPart(watchingSlot) ? 0 : 15;
		if (newOutput != output)
		{
			output = newOutput;
			FacBlockHelper.updateNeighbours(world, pos);
//			world.notifyNeighborsOfStateChange(pos, BlockRegistry.partSensor, false);
		}
//		if (!getMachine().hasPart(watchingSlot)) output = 0;
//		else output = 15;
	}
	
	@Override
	public int getComparatorOverride()
	{
		return output;
	}
	
	@Override
	public List<String> getGlassInfo()
	{
		List<String> info = new ArrayList<>();
		if (getMachine() == null) info.add("Not watching machine");
		else info.add(String.format("Watching machine %s, part slot %s", getMachine().getName(), watchingSlot));
		return info;
	}
}