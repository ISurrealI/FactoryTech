package dalapo.factech.tileentity.automation;

import java.util.UUID;

import com.mojang.authlib.GameProfile;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.WorldServer;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.util.BlockSnapshot;
import net.minecraftforge.common.util.FakePlayerFactory;
import net.minecraftforge.event.world.BlockEvent.PlaceEvent;
import dalapo.factech.helper.FacMathHelper;
import dalapo.factech.helper.FacMiscHelper;
import dalapo.factech.reference.StateList;
import dalapo.factech.tileentity.ActionOnRedstone;
import dalapo.factech.tileentity.TileEntityBasicInventory;

// Copypasta from AutoUtils, edited to work in 1.12.2
// Known bug: Shulker Boxes are... glitchy
public class TileEntitySequencePlacer extends TileEntityBasicInventory implements ActionOnRedstone
{
	private int nextSlot;
	private boolean isPowered = false;
	private EntityPlayer fakePlayer;
	
	public TileEntitySequencePlacer() {
		super("sequenceplacer", 18);
		nextSlot = 0;
	}
	
	public void onLoad()
	{
		if (!world.isRemote)
		{
			fakePlayer = FacMiscHelper.getFakePlayer(world);
		}
	}
	
	private void placeNextBlock()
	{
		EnumFacing front = world.getBlockState(pos).getValue(StateList.directions);
		if (!world.isAirBlock(FacMathHelper.withOffset(pos, front))) return;
		ItemStack itemstack = getStackInSlot(nextSlot).copy();
		boolean flag = false;
		for (int i=0; i<18; i++)
		{
			itemstack = getStackInSlot(nextSlot).copy();
			if (itemstack != null && itemstack.getItem() instanceof ItemBlock)
			{
				flag = true;
				decrStackSize(nextSlot, 1);
			}
			nextSlot++;
			if (nextSlot > 17) nextSlot = 0;
			if (flag) break;
		}
		if (flag)
		{
			Block block = ((ItemBlock)itemstack.getItem()).getBlock();
			IBlockState state = block.getStateFromMeta(itemstack.getItemDamage());
			BlockPos toPlace = FacMathHelper.withOffset(pos, front);
			BlockSnapshot snapshot = new BlockSnapshot(world, pos, state);
			if (!MinecraftForge.EVENT_BUS.post(new PlaceEvent(snapshot, world.getBlockState(pos), fakePlayer, EnumHand.MAIN_HAND)))
			{
				world.setBlockState(FacMathHelper.withOffset(pos, front), block.getStateFromMeta(itemstack.getItemDamage()));	
			}
		}
	}

	@Override
	public void onRedstoneSignal(boolean signal, EnumFacing side) {
		if (world.isBlockPowered(getPos()))
		{
			if (!isPowered)
			{
				placeNextBlock();
				isPowered = true;
			}
		}
		else isPowered = false;
		
	}
	
	@Override
	public boolean isItemValidForSlot(int slot, ItemStack itemstack)
	{
		return itemstack.getItem() instanceof ItemBlock;
	}
	
	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound nbt)
	{
		super.writeToNBT(nbt);
		nbt.setInteger("nextSlot", nextSlot);
		return nbt;
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);
		nextSlot = nbt.getInteger("nextSlot");
	}

	@Override
	public int getField(int id) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setField(int id, int value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getFieldCount() {
		// TODO Auto-generated method stub
		return 0;
	}
}