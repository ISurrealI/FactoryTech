package dalapo.factech.tileentity.automation;

import dalapo.factech.helper.FacMathHelper;
import dalapo.factech.helper.FacStackHelper;
import dalapo.factech.helper.Logger;
import dalapo.factech.reference.StateList;
import dalapo.factech.tileentity.TileEntityItemQueue;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;

public class TileEntityConveyor extends TileEntityItemQueue
{
	public TileEntityConveyor()
	{
		super("conveyor");
	}
	
	public TileEntityConveyor(String name)
	{
		super(name);
	}
	
	@Override
	public int getCapacity()
	{
		return 20;
	}
	
	@Override
	public void update()
	{
		if (targetPos.getY() != pos.getY()) targetPos = getTarget(); // fine
		super.update();
	}
	
	@Override
	public BlockPos getTarget()
	{
		return FacMathHelper.withOffset(pos, world.getBlockState(pos).getValue(StateList.directions).getOpposite());
	}

	@Override
	protected void ejectItem(ItemStack toEject)
	{
		FacStackHelper.spawnEntityItem(world, toEject, getTarget(), true);
	}
}