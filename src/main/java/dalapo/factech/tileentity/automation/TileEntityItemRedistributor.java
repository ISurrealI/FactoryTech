package dalapo.factech.tileentity.automation;

import java.util.HashMap;
import java.util.Map;

import dalapo.factech.tileentity.TileEntityBase;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.items.IItemHandler;

public class TileEntityItemRedistributor extends TileEntityBase
{
	private Map<EnumFacing, IItemHandler> inventories = new HashMap<>();
	
	public TileEntityItemRedistributor(String name)
	{
		super(name);
	}
}