package dalapo.factech.tileentity.automation;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import dalapo.factech.helper.FacMathHelper;
import dalapo.factech.helper.FacMiscHelper;
import dalapo.factech.reference.StateList;
import dalapo.factech.tileentity.IMagnifyingGlassInfo;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandlerItem;
import net.minecraftforge.fluids.capability.TileFluidHandler;

public class TileEntityPipeValve extends TileEntityPipe
{
	public static final int MAX_FLOW = 50; // Equivalent to regular pipes
	// Fields
	private Fluid filter;
	private int flowLimit = 0;
	private EnumFacing inputDirection; // Output is opposite
	private boolean redstoneControlled = false;
	
	public int getFlow()
	{
		return Math.min(flowLimit, MAX_FLOW);
	}
    
	@Override
	public void onLoad()
	{
		super.onLoad();
		inputDirection = world.getBlockState(pos).getValue(StateList.directions);
	}
	
	@Override
	public int insertFluid(@Nonnull FluidStack toTransfer, boolean doFill)
	{
		if (filter == null || filter.equals(toTransfer.getFluid()))
		{
			return tank.fillInternal(toTransfer, doFill);
		}
		return 0;
	}
	
	@Override
	public void propagateFluid()
	{
		if (inputDirection == null) inputDirection = world.getBlockState(pos).getValue(StateList.directions);
		EnumFacing outputDirection = inputDirection.getOpposite();
		prevFilled = outputDirection;
		if (this.tank.getFluidAmount() == 0 || (redstoneControlled && world.isBlockIndirectlyGettingPowered(pos) > 0)) return;
		
		TileEntity te = world.getTileEntity(FacMathHelper.withOffset(getPos(), outputDirection));
		if (te != null && te.hasCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY, inputDirection))
		{
			IFluidHandler tank = te.getCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY, inputDirection);
			FluidStack toTransfer = this.tank.drain(getFlow(), false);
			int filled = 0;
			if (toTransfer == null) return;
			if (te instanceof TileEntityPipe)
			{
				filled = ((TileEntityPipe)te).insertFluid(toTransfer, true);
				((TileEntityPipe)te).prevFilled = inputDirection;
			}
			else
			{
				filled = tank.fill(toTransfer, true);
			}
			this.tank.drain(filled, true);
			te.markDirty();
		}
	}
	
	@Override
    public boolean hasCapability(@Nonnull Capability<?> capability, @Nullable EnumFacing facing)
    {
        return capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY && (facing == inputDirection || facing == inputDirection.getOpposite());
    }

    @Override
    @Nullable
    public <T> T getCapability(@Nonnull Capability<T> capability, @Nullable EnumFacing facing)
    {
        if (capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY && facing == inputDirection || facing == inputDirection.getOpposite())
            return (T) tank;
        return super.getCapability(capability, facing);
    }
    
    @Override
    public boolean onRightClick(EntityPlayer ep, EnumFacing dir, ItemStack held)
    {
    	IFluidHandlerItem handler = held.getCapability(CapabilityFluidHandler.FLUID_HANDLER_ITEM_CAPABILITY, null);
    	if (handler != null)
    	{
    		FluidStack fs = handler.getTankProperties()[0].getContents();
    		if (fs == null || fs.getFluid() == null) filter = null;
    		else filter = fs.getFluid();
    		return true;
    	}
    	return false;
    }
    
    @Override
    public int getVal(int id)
    {
    	switch (id)
    	{
    	case 0:
    		return flowLimit;
    	case 1:
    		return redstoneControlled ? 1 : 0;
    		default:
    			return 0;
    	}
    }
    
    @Override
    public void setVal(int id, int num)
    {
    	switch (id)
    	{
    	case 0:
    		flowLimit = (int)FacMathHelper.clamp(num, 0, MAX_FLOW);
    	case 1:
    		redstoneControlled = num == 0 ? false : true;
    	}
    	markDirty();
    }
    
    @Override
	public List<String> getGlassInfo()
    {
		List<String> info = new ArrayList<>();
		info.add(FacMiscHelper.describeTank(tank));
		info.add(String.format("Flow limit: %s", Math.min(flowLimit, MAX_FLOW)));
		info.add(filter == null ? "No filter" : String.format("Only allowing %s", filter.getLocalizedName(null))); // No idea why Fluid.getLocalizedName takes a FluidStack arg
		return info;
	}
    
    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound nbt)
    {
    	super.writeToNBT(nbt);
    	nbt.setInteger("flowLimit", flowLimit);
    	nbt.setBoolean("redstone", redstoneControlled);
    	nbt.setString("filter", filter == null ? "[none]" : filter.getName());
    	return nbt;
    }
    
    @Override
    public void readFromNBT(NBTTagCompound nbt)
    {
    	super.readFromNBT(nbt);
    	flowLimit = nbt.getInteger("flowLimit");
    	redstoneControlled = nbt.getBoolean("redstone");
    	String f = nbt.getString("filter");
    	if (f == null || f.equals("[none]")) filter = null;
    	else filter = FluidRegistry.getFluid(f);
    }
}