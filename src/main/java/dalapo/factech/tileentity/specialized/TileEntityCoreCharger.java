package dalapo.factech.tileentity.specialized;

import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import dalapo.factech.init.ItemRegistry;
import dalapo.factech.reference.PartList;
import dalapo.factech.tileentity.TileEntityMachine;

public class TileEntityCoreCharger extends TileEntityMachine
{

	public TileEntityCoreCharger()
	{
		super("corecharger", 1, 4, 0);
		isDisabledByRedstone = true;
		io = new IOStackHandler(1, 0, this)
		{
			@Override
			public ItemStack extractItem(int slot, int amount, boolean simulate)
			{
				return extractItemInternal(slot, amount, simulate);
			}
		};
	}
	
//	@Override
//	public boolean canExtractItem(int index, ItemStack stack, EnumFacing direction) {
//		return (index == 0);
//	}
	
	@Override
	protected boolean performAction() {
		if (!getInput().getItem().equals(ItemRegistry.coreUnfinished)) return false;
		
		// Explode if the core is overcharged
		if (getInput().getItemDamage() <= 1)
		{
			getIO().setStackInSlot(0, ItemStack.EMPTY);
			for (int i=0; i<4; i++)
			{
				getParts().setStackInSlot(0, ItemStack.EMPTY);
			}
			world.createExplosion(null, pos.getX() + 0.5, pos.getY() + 0.5, pos.getZ() + 0.5, 3, true);
		}
		else
		{
			getInput().setItemDamage(getInput().getItemDamage() - 2);
		}
		return true;
	}

	@Override
	public void getHasWork()
	{
		if (getInput().getItem() == ItemRegistry.coreUnfinished) hasWork = true;
		else hasWork = false;
	}
	
	@Override
	public int getBaseOpTicks() {
		return 10;
	}
}
