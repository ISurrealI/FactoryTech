package dalapo.factech.tileentity.specialized;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

import dalapo.factech.auxiliary.MachineRecipes;
import dalapo.factech.auxiliary.MachineRecipes.MachineRecipe;
import dalapo.factech.helper.FacStackHelper;
import dalapo.factech.helper.Pair;
import dalapo.factech.init.ItemRegistry;
import dalapo.factech.init.ModFluidRegistry;
import dalapo.factech.reference.PartList;
import dalapo.factech.tileentity.TileEntityFluidMachine;
import dalapo.factech.tileentity.TileEntityMachine;

// Apparently, electrorefining doesn't work on iron or nickel. Oh well. I'm not above bending the rules a bit.
public class TileEntityElectroplater extends TileEntityFluidMachine {
	
	private ItemStack out;
	public TileEntityElectroplater() {
		super("electroplater", 2, 3, 1, 1, 1);
	}

	@Override
	public boolean canRun()
	{
		return super.canRun() && hasWork;
	}
	@Override
	public void getHasWork()
	{
		if (tanks[0].getFluid() == null || !tanks[0].getFluid().getFluid().equals(ModFluidRegistry.h2so4))
		{
			hasWork = false;
			return;
		}
		if (tanks[0].getFluidAmount() < 200 || getInput().isEmpty() || getInput(1).isEmpty())
		{
			hasWork = false;
			return;
		}
		for (MachineRecipe<ItemStack, ItemStack> entry : MachineRecipes.ELECTROPLATER)
		{
			if (FacStackHelper.matchStacksWithWildcard(getInput(), entry.input(), true) && getInput(1).isItemEqual(entry.output()) && getOutput().getCount() < 64)
			{
				hasWork = true;
				out = entry.output();
				return;
			}
		}
		hasWork = false;
	}

	@Override
	protected boolean performAction() {
		tanks[0].drainInternal(250, true);
		doOutput(out.copy());
		getInput().shrink(1);
		getHasWork();
		return true;
	}

	@Override
	public int getBaseOpTicks() {
		// 16 seconds. Electrowinning is slow.
		return 320;
	}

	
}
