package dalapo.factech.tileentity.specialized;

import dalapo.factech.reference.AABBList;
import dalapo.factech.reference.StateList;
import dalapo.factech.tileentity.TileEntityBase;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.AxisAlignedBB;

import static dalapo.factech.FactoryTech.random;

public class TileEntityMobFan extends TileEntityBase implements ITickable
{
	private boolean hasLoaded = false;
	private EnumFacing dir;
	private AxisAlignedBB pushBox;
	public TileEntityMobFan()
	{
		super("mobfan");
	}

	@Override
	public void markDirty()
	{
		hasLoaded = false;
		super.markDirty();
	}
	
	@Override
	public void update()
	{
		if (!hasLoaded)
		{
			dir = world.getBlockState(pos).getValue(StateList.directions);
			pushBox = AABBList.BLOCK.offset(pos).expand(dir.getFrontOffsetX()*3, dir.getFrontOffsetY()*3, dir.getFrontOffsetZ()*3);
			hasLoaded = true;
		}
		if (world.isBlockIndirectlyGettingPowered(pos) == 0)
		{
			for (EntityLivingBase e : world.getEntitiesWithinAABB(EntityLivingBase.class, pushBox))
			{
				if (!(e instanceof EntityPlayer && ((EntityPlayer)e).isCreative()))
				{
					e.motionX += dir.getFrontOffsetX()*0.1;
					e.motionY += dir.getFrontOffsetY();
					e.motionZ += dir.getFrontOffsetZ()*0.1;
				}
			}
			if (world.isRemote && random.nextInt(3) == 0)
			{
				world.spawnParticle(EnumParticleTypes.CLOUD, pos.getX()+0.5+(0.25*random.nextGaussian()), pos.getY()+0.5+(0.25*random.nextGaussian()), pos.getZ()+0.5+(0.25*random.nextGaussian()), dir.getFrontOffsetX()*0.25, 0, dir.getFrontOffsetZ()*0.25);
			}
		}
	}
}