package dalapo.factech.tileentity.specialized;

import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import dalapo.factech.auxiliary.MachineRecipes;
import dalapo.factech.auxiliary.MachineRecipes.MachineRecipe;
import dalapo.factech.helper.FacStackHelper;
import dalapo.factech.helper.Logger;
import dalapo.factech.helper.Pair;
import dalapo.factech.init.ModFluidRegistry;
import dalapo.factech.tileentity.TileEntityFluidMachine;
import dalapo.factech.tileentity.TileEntityMachine;
import dalapo.factech.tileentity.TileEntityProcessorMultiOutput;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;

public class TileEntityMagnetCentrifuge extends /* TileEntityProcessorMultiOutput */ TileEntityFluidMachine
{
	public TileEntityMagnetCentrifuge()
	{
		super("magcent", 1, 3, 3, 1, 1);
	}

	@Override
	public void getHasWork()
	{
		Pair<Integer, ItemStack[]> data = getOutput(getInput(0));
		if (data == null)
		{
			hasWork = false;
			return;
		}
		ItemStack[] is = data.b;
		try {
			for (int i=0; i<is.length; i++)
			{
				if (!(is[i].isItemEqual(getOutput(i)) || getOutput(i).isEmpty()) ||
						getInput().getCount() < data.a || 
						is[i].isEmpty() || is[i].getCount() + getOutput(i).getCount() >= 64)
				{
					hasWork = false;
					return;
				}
			}
			hasWork = getTank(0).getFluidAmount() > 250 && getTank(0).getFluid().getFluid() == FluidRegistry.WATER;
		}
		catch (ArrayIndexOutOfBoundsException e)
		{
			Logger.error(String.format("Machine %s attempted to output to a nonexistent slot: %s", this, e.getMessage()));
		}
	}
	
	@Override
	protected boolean performAction() {
		Pair<Integer, ItemStack[]> data = getOutput(getInput(0));
		ItemStack[] output = data.b;
		getInput(0).shrink(data.a);
		if (output.length > outputSlots)
		{
			Logger.error("Who the hell designed this thing?");
			return false;
		}
		for (int i=0; i<output.length; i++)
		{
			Logger.debug("Outputting " + output[i]);
			doOutput(output[i].copy());
		}
		getTank(0).drainInternal(250, true);
		getHasWork();
		markDirty();
		return true;
	}
	
	protected Pair<Integer, ItemStack[]> getOutput(ItemStack is)
	{
		for (MachineRecipe<ItemStack, ItemStack[]> entry : getRecipeList())
		{
			if (hasBadParts() && !entry.worksWithBad()) continue;
			ItemStack in = entry.input().copy();
			ItemStack[] out = new ItemStack[entry.output().length];
			if (FacStackHelper.matchStacksWithWildcard(in, is) && in.getCount() <= is.getCount())
			{
				for (int i=0; i<out.length; i++)
				{
					out[i] = entry.output()[i].copy();
				}
				return new Pair<Integer, ItemStack[]>(entry.input().getCount(), out);
			}
		}
		return null;
	}
	
	@Override
	public int getBaseOpTicks()
	{
		return 80;
	}

	public List<MachineRecipe<ItemStack, ItemStack[]>> getRecipeList()
	{
		return MachineRecipes.MAGNET_CENTRIFUGE;
	}

}

//public class TileEntityMagnetCentrifuge extends TileEntityFluidMachine
//{
//	public TileEntityMagnetCentrifuge()
//	{
//		super("magcent", 1, 3, 3, 1, 1, RelativeSide.BACK);
//		setDisplayName("Magnetic Centrifuge");
//	}
//
//	// Temporary implementation, of course
//	public Pair<Integer, ItemStack[]> getOutput(ItemStack in)
//	{
//		return new Pair(0, new ItemStack[] {ItemStack.EMPTY, ItemStack.EMPTY, ItemStack.EMPTY});
//	}
//	
//	@Override
//	public int getOpTime()
//	{
//		return 80;
//	}
//
//	public void getHasWork()
//	{
//		FluidStack fs = getTankContents(0);
//		if (fs != null && fs.amount >= 1000)
//		{
//			for (MachineRecipe<FluidStack, ItemStack[]> mr : MachineRecipes.MAGNET_CENTRIFUGE_NEW)
//			{
//				if (fs.isFluidEqual(mr.input()))
//				{
//					for (int i=0; i<3; i++)
//					{
//						ItemStack is = getOutput(i);
//						if (!FacStackHelper.canCombineStacks(is, mr.output()[i]))
//						{
//							hasWork = false;
//							return;
//						}
//					}
//					hasWork = true;
//					return;
//				}
//			}
//			hasWork = false;
//			return;
//		}
//	}
//	/*
//	public void getHasWork()
//	{
//		Pair<Integer, ItemStack[]> data = getOutput(getInput(0));
//		if (data == null)
//		{
//			hasWork = false;
//			return;
//		}
//		ItemStack[] is = data.b;
//		try {
//			for (int i=0; i<is.length; i++)
//			{
//				if (!(is[i].isItemEqual(getOutput(i)) || getOutput(i).isEmpty()) ||
//						getInput().getCount() < data.a || 
//						is[i].isEmpty() || is[i].getCount() + getOutput(i).getCount() >= 64)
//				{
//					hasWork = false;
//					return;
//				}
//			}
//			hasWork = true;
//		}
//		catch (ArrayIndexOutOfBoundsException e)
//		{
//			Logger.error(String.format("Machine %s attempted to output to a nonexistent slot: %s", this, e.getMessage()));
//		}
//	}
//	*/
//	
//	@Override
//	protected boolean performAction() {
//		return false;
//	}
//
//	public static class MagCentRecipe
//	{
//		public @Nullable FluidStack fluid;
//		public ItemStack inputItem;
//		public ItemStack[] outputs = new ItemStack[3];
//		
//		public MagCentRecipe(@Nullable FluidStack fs, ItemStack input, ItemStack... outputs)
//		{
//			
//		}
//	}
//}