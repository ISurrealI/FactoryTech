package dalapo.factech.tileentity;

import static dalapo.factech.FactoryTech.DEBUG_PACKETS;

import javax.annotation.Nullable;

import dalapo.factech.block.BlockDirectional;
import dalapo.factech.helper.FacBlockHelper;
import dalapo.factech.helper.Logger;
import dalapo.factech.reference.StateList;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public abstract class TileEntityBase extends TileEntity {
	private final String name;
	@Nullable protected EnumFacing direction;
	
	public static boolean isReversed = false;
	
	public TileEntityBase(String name)
	{
		this.name = name;
	}
//	public abstract void sendInfoPacket(EntityPlayer ep);
	
	public String getName()
	{
		return name;
	}
	
	public void setDirection(EnumFacing dir)
	{
		this.direction = dir;
	}
	
	public EnumFacing getDirection() // direction may be null
	{
		return direction;
	}
	
	public boolean isPowered()
	{
		return world.isBlockIndirectlyGettingPowered(pos) > 0;
	}
	
	public boolean isReceivingPowerFrom(EnumFacing side)
	{
		return world.getRedstonePower(pos.offset(side), side) > 0;
	}
	
	protected void markDirtyLight()
	{
		if (world != null)
		{
			world.markChunkDirty(pos, this);
		}
	}
	
	@Override
	public NBTTagCompound getUpdateTag()
	{
		if (DEBUG_PACKETS)
		{
			Logger.info("Entered getUpdateTag()");
			Thread.dumpStack();
		}
		NBTTagCompound nbt = super.getUpdateTag();
		return writeToNBT(nbt);
	}
	
	@Override
	public SPacketUpdateTileEntity getUpdatePacket()
	{
		if (DEBUG_PACKETS)
		{
			Logger.info(String.format("Entered getUpdatePacket; thread = %s", Thread.currentThread()));
			Thread.dumpStack();
		}
		NBTTagCompound nbt = new NBTTagCompound();
		this.writeToNBT(nbt);
		return new SPacketUpdateTileEntity(getPos(), 1, nbt);
	}
	
	@Override
	public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity packet)
	{
//		Logger.info(String.format("Entered onDataPacket; thread = %s", Thread.currentThread()));
		this.readFromNBT(packet.getNbtCompound());
	}
	
	@Override
	public boolean shouldRefresh(World world, BlockPos pos, IBlockState oldState, IBlockState newState)
	{
		return oldState.getBlock() != newState.getBlock();
	}
	
	// Has to be done this way because getField and setField are defined in IInventory instead of TileEntity
	// Any subclass of IInventory will just have those methods call these ones
	public int getVal(int id) { return 0; }
	public void setVal(int id, int val) {}
	public int getValCount() { return 0; }
	public int getMaxVal(int id) { return 0; }
	
	public void toggleVal(int id)
	{
		if (getVal(id) == 0) setVal(id, 1);
		else setVal(id, 0);
	}

	public boolean isUsableByPlayer(EntityPlayer ep)
	{
		return true;
	}
	
	public void onNeighbourChange(BlockPos changedPos)
	{
	}
	
	protected void sync()
	{
		FacBlockHelper.updateBlock(world, pos);
	}
	
	@Override
	public ITextComponent getDisplayName()
	{
		return new TextComponentString(I18n.format("factorytech.display." + name));
	}
	
	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound nbt)
	{
		super.writeToNBT(nbt);
		if (direction != null) nbt.setInteger("direction", direction.getIndex());
		return nbt;
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);
		if (nbt.hasKey("direction")) direction = EnumFacing.getFront(nbt.getInteger("direction"));
		else direction = null;
	}
	
	/**
	 * Called when the block this TE is attached to is right-clicked.
	 * @param ep
	 * @param is
	 * @return True if the action done by this function was successful; this will prevent any GUI from opening.
	 */
	public boolean onRightClick(EntityPlayer ep, EnumFacing side, ItemStack is)
	{
		return false;
	}

	public int getComparatorOverride()
	{
		return 0;
	}
}