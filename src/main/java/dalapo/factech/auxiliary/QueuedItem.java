package dalapo.factech.auxiliary;

import dalapo.factech.tileentity.TileEntityItemQueue;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;

public class QueuedItem {
	private ItemStack item;
	private int timeInQueue;
	private TileEntityItemQueue queue;
	private boolean isMoving;
	
	public static final QueuedItem EMPTY = new QueuedItem(ItemStack.EMPTY, null, -1);
	
	public QueuedItem(ItemStack is, TileEntityItemQueue q)
	{
		item = is;
		queue = q;
		timeInQueue = 0;
	}
	
	public QueuedItem(ItemStack is, TileEntityItemQueue q, int startingTime)
	{
		item = is;
		queue = q;
		timeInQueue = startingTime;
	}
	
	public QueuedItem(NBTTagCompound nbt, TileEntityItemQueue q)
	{
		deserializeNBT(nbt);
		queue = q;
	}
	/**
	 * You probably shouldn't modify this ItemStack either
	 * @return
	 */
	public ItemStack getItem()
	{
		return item;
	}
	
	public int getTime()
	{
		return timeInQueue;
	}
	
	public void setTime(int time)
	{
		timeInQueue = time;
	}
	
	public boolean tick()
	{
		return (++timeInQueue >= queue.getCapacity());
	}
	
	public NBTTagCompound serializeNBT()
	{
		NBTTagCompound nbt = item.serializeNBT();
		nbt.setInteger("timeInQueue", timeInQueue);
		return nbt;
	}
	
	public void deserializeNBT(NBTTagCompound nbt)
	{
		item = new ItemStack(nbt);
		timeInQueue = nbt.hasKey("timeInQueue") ? nbt.getInteger("timeInQueue") : 0;
	}
	
	@Override
	public boolean equals(Object o)
	{
		if (!(o instanceof QueuedItem)) return false;
		return ((QueuedItem)o).item.equals(item) && ((QueuedItem)o).timeInQueue == timeInQueue;
	}
}