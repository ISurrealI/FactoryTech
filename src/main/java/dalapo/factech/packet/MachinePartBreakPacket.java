package dalapo.factech.packet;

import dalapo.factech.tileentity.TileEntityMachine;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class MachinePartBreakPacket extends FacTechPacket
{
	private BlockPos pos;
	private int id;
	
	public MachinePartBreakPacket(BlockPos bp, int i)
	{
		this.pos = bp;
		this.id = i;
	}
	
	public MachinePartBreakPacket() {}
	
	@Override
	protected void actuallyDoHandle(FacTechPacket msg, World world, EntityPlayer ep, boolean isClient)
	{
		MachinePartBreakPacket packet = (MachinePartBreakPacket)msg;
		TileEntityMachine te = (TileEntityMachine)world.getTileEntity(packet.pos);
		te.breakPart(id);
	}

	@Override
	public void fromBytes(ByteBuf buf)
	{
		pos = BlockPos.fromLong(buf.readLong());
		id = buf.readInt();
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		buf.writeLong(pos.toLong());
		buf.writeInt(id);
	}
}