package dalapo.factech.plugins.crafttweaker;

import java.util.ArrayList;
import java.util.List;

import dalapo.factech.auxiliary.IMachineRecipe;
import dalapo.factech.auxiliary.MachineRecipes;
import dalapo.factech.auxiliary.MachineRecipes.MachineRecipe;
import dalapo.factech.helper.FacStackHelper;
import dalapo.factech.helper.Pair;
import dalapo.factech.tileentity.specialized.TileEntityAgitator.AgitatorRecipe;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fluids.FluidStack;

public class ScheduledRemovals
{
	private List<Pair<List, Integer>> removals = new ArrayList<>();
//	private List<Pair<List, FluidStack>> fluidRemovals = new ArrayList<>();
//	private List<Pair<ItemStack, FluidStack>> agitatorRemovals = new ArrayList<>();
	
	public static final ScheduledRemovals INSTANCE = new ScheduledRemovals();
	
	public void scheduleAllRemovals(List<? extends IMachineRecipe> list)
	{
		for (int i=0; i<list.size(); i++)
		{
			removals.add(new Pair<>(list, i));
		}
	}
	
	public void scheduleRemoval(List<? extends IMachineRecipe> list, int index)
	{
		removals.add(new Pair<>(list, index));
	}
	
	public void scheduleRemoval(List<? extends IMachineRecipe<ItemStack>> list, ItemStack is)
	{
		for (int i=0; i<list.size(); i++)
		{
			IMachineRecipe<ItemStack> recipe = list.get(i);
			ItemStack stack = recipe.getOutputStack();
			if (FacStackHelper.areItemStacksIdentical(stack, is)) scheduleRemoval(list, i);
		}
	}
	
	public void scheduleRemoval(List<? extends IMachineRecipe<FluidStack>> list, FluidStack fs)
	{
		for (int i=0; i<list.size(); i++)
		{
			IMachineRecipe<FluidStack> recipe = list.get(i);
			FluidStack stack = recipe.getOutputStack();
			if (FacStackHelper.areFluidStacksIdentical(stack, fs)) scheduleRemoval(list, i);
		}
	}
	
	public void removeAll()
	{
		for (Pair<List, Integer> p : removals)
		{
			p.a.set(p.b, null);
		}
		for (Pair<List, Integer> p : removals)
		{
			p.a.removeIf(recipe -> recipe == null);
		}
	}
}
