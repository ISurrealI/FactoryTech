package dalapo.factech.plugins.crafttweaker;

import java.util.Map.Entry;

import javax.annotation.Nullable;

import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.fluids.FluidStack;
import crafttweaker.CraftTweakerAPI;
import crafttweaker.IAction;
import crafttweaker.annotations.ZenRegister;
import crafttweaker.api.item.IIngredient;
import crafttweaker.api.item.IItemStack;
import crafttweaker.api.liquid.ILiquidStack;
import dalapo.factech.auxiliary.MachineRecipes;
import dalapo.factech.auxiliary.MachineRecipes.MachineRecipe;
import dalapo.factech.helper.FacStackHelper;
import dalapo.factech.helper.Pair;
import dalapo.factech.tileentity.specialized.TileEntityCompressionChamber.CompressorRecipe;
import stanhebben.zenscript.annotations.Optional;
import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;

@ZenClass("mods.factorytech.CompressionChamber")
@ZenRegister
public class CompressionChamber
{
	@ZenMethod
	public static void addRecipe(IItemStack output, IIngredient input, @Optional ILiquidStack fluidIn)
	{
		FluidStack fs = fluidIn == null ? null : (FluidStack)fluidIn.getInternal();
		for (IItemStack itemIn : input.getItems())
		{
			CraftTweakerAPI.apply(new Add((ItemStack)itemIn.getInternal(), fs, (ItemStack)output.getInternal()));
		}
	}
	
	@ZenMethod
	public static void removeRecipe(IIngredient output)
	{
		for (IItemStack out : output.getItems())
		{
			CraftTweakerAPI.apply(new Remove((ItemStack)output.getInternal()));
		}
	}
	
	@ZenMethod
	public static void removeAll()
	{
		CraftTweakerAPI.apply(new RemoveAll(MachineRecipes.COMPRESSOR, "Compression Chamber"));
	}
	
	private static class Add implements IAction
	{
		private CompressorRecipe recipe;
		
		public Add(ItemStack itemIn, FluidStack fluidIn, ItemStack out)
		{
			recipe = new CompressorRecipe(itemIn, fluidIn, out);
		}
		
		@Override
		public void apply()
		{
			ScheduledAdditions.scheduleAddition(MachineRecipes.COMPRESSOR, recipe);
		}

		@Override
		public String describe() {
			// TODO Auto-generated method stub
			return "Adding Compressor recipe: " + recipe.describe();
		}
	}
	
	private static class Remove implements IAction
	{
		ItemStack output;
		
		public Remove(ItemStack o)
		{
			this.output = o;
		}
		
		@Override
		public void apply()
		{
			ScheduledRemovals.INSTANCE.scheduleRemoval(MachineRecipes.COMPRESSOR, output);
		}

		@Override
		public String describe() {
			return "Removing Refrigerator recipe for " + output;
		}
		
	}
}