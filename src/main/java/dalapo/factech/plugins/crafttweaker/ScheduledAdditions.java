package dalapo.factech.plugins.crafttweaker;

import java.util.ArrayList;
import java.util.List;

import dalapo.factech.auxiliary.IMachineRecipe;
import dalapo.factech.auxiliary.MachineRecipes.MachineRecipe;
import dalapo.factech.helper.Logger;
import dalapo.factech.helper.Pair;

public class ScheduledAdditions
{
	public static List<Pair<List, IMachineRecipe>> additions = new ArrayList<>();
	
	public static void scheduleAddition(List list, IMachineRecipe recipe)
	{
		additions.add(new Pair<List, IMachineRecipe>(list, recipe));
	}
	
	public static void addAll()
	{
		additions.forEach(p -> {
			Logger.info("Logged addition of " + p.b);
			p.a.add(p.b);
		});
	}
}