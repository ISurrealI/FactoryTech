package dalapo.factech.reference;

import dalapo.factech.tileentity.TileEntityBase;
import dalapo.factech.tileentity.automation.*;
import dalapo.factech.tileentity.specialized.*;
import net.minecraft.tileentity.TileEntity;

public class TileReference {
	private TileReference() {}
	
	public static TileEntityBase getTileFromID(String id)
	{
		switch (id)
		{
		case "potionmixer":
			return new TileEntityPotionMixer();
		case "saw":
			return new TileEntitySaw();
		case "disruptor":
			return new TileEntityDisruptor();
		case "stackmover":
			return new TileEntityStackMover();
		case "filtermover":
			return new TileEntityFilterMover();
		case "bulkmover":
			return new TileEntityBulkMover();
		case "autopuller":
			return new TileEntityAutoPuller();
		case "htfurnace":
			return new TileEntityHTFurnace();
		case "oredrill":
			return new TileEntityOreDrill();
		case "autocrafter":
			return new TileEntityAutoCrafter();
		case "crucible":
			return new TileEntityCrucible();
		case "fluidpuller":
			return new TileEntityFluidPuller();
		case "grindstone":
			return new TileEntityGrindstone();
		case "metalcutter":
			return new TileEntityMetalCutter();
		case "itempusher":
			return new TileEntityItemPusher();
		case "itemredis":
			return new TileEntityItemRedis();
		case "circuitscribe":
			return new TileEntityCircuitScribe();
		case "centrifuge":
			return new TileEntityCentrifuge();
		case "fluiddrill":
			return new TileEntityFluidDrill();
		case "agitator":
			return new TileEntityAgitator();
		case "sluice":
			return new TileEntitySluice();
		case "miner":
			return new TileEntityAutoMiner();
		case "electroplater":
			return new TileEntityElectroplater();
		case "stabilizer":
			return new TileEntityStabilizer();
		case "magnetblock":
			return new TileEntityMagnet();
		case "elevator":
			return new TileEntityLiftFan();
		case "magnetizer":
			return new TileEntityMagnetizer();
		case "charger":
			return new TileEntityCoreCharger();
		case "compressor":
			return new TileEntityCompressionChamber();
		case "spawner":
			return new TileEntitySpawner();
		case "disassembler":
			return new TileEntityDisassembler();
		case "decocoil":
			return new TileEntityDecoCoil();
		case "watercollector":
			return new TileEntityWaterCollector();
		case "crate":
			return new TileEntityCrate("crate");
		case "energizer":
			return new TileEntityEnergizer();
		case "fridge":
			return new TileEntityRefrigerator();
		case "propfurnace":
			return new TileEntityPropaneFurnace();
		case "woodcutter":
			return new TileEntityWoodcutter();
		case "teslacoil":
			return new TileEntityTeslaCoil();
		case "iondisperser":
			return new TileEntityIonDisperser();
		case "sequenceplacer":
			return new TileEntitySequencePlacer();
		case "tankblock":
			return new TileEntityTank();
		case "temperer":
			return new TileEntityTemperer();
		case "blockbreaker":
			return new TileEntityBlockBreaker();
		case "deepdrill":
			return new TileEntityDeepDrill();
		case "planter":
			return new TileEntityPlanter();
		case "buffercrate":
			return new TileEntityBufferCrate();
		case "inventorysensor":
			return new TileEntityInventorySensor();
		case "interceptor":
			return new TileEntityItemInterceptor();
		case "aerolyzer":
			return new TileEntityAerolyzer();
		case "magcent":
			return new TileEntityMagnetCentrifuge();
		case "planeshifter":
			return new TileEntityPlaneShifter();
		case "realelevator":
			return new TileEntityElevator();
		case "pulser":
			return new TileEntityPulser();
		case "pulsecounter":
			return new TileEntityPulseCounter();
		case "batterygen":
			return new TileEntityBatteryGenerator();
		case "coregen":
			return new TileEntityCoreGenerator();
		case "conveyor":
			return new TileEntityConveyor();
		case "blowtorch":
			return new TileEntityBlowtorch();
		case "reclaimer":
			return new TileEntityReclaimer();
		case "mobfan":
			return new TileEntityMobFan();
		case "compacthopper":
			return new TileEntityCompactHopper();
		case "valve":
			return new TileEntityPipeValve();
		case "remotecomparator":
			return new TileEntityRemoteComparator();
		case "partsensor":
			return new TileEntityPartSensor();
		case "trapdoorconveyor":
			return new TileEntityTrapdoorConveyor();
		default:
			return null;
		}
	}
}
