package dalapo.factech.block;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Nonnull;

import dalapo.factech.auxiliary.Wrenchable;
import dalapo.factech.helper.FacBlockHelper;
import dalapo.factech.helper.FacMathHelper;
import dalapo.factech.helper.Logger;
import dalapo.factech.reference.StateList;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.properties.PropertyDirection;
import net.minecraft.block.properties.PropertyInteger;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockDirectional extends BlockBase implements Wrenchable
{
	private boolean useAlternatePlacementMethod = false;
	protected boolean planeLocked;
	public Set<EnumFacing> directionBlacklist = new HashSet<>();
	public static final PropertyDirection FACING = PropertyDirection.create("facing");
	public static final PropertyInteger PART_ID = PropertyInteger.create("partid", 0, 16); // 16 moving parts ought to be enough for anyone

	public BlockDirectional(Material materialIn, String name, boolean locked) {
		super(materialIn, name);
		setDefaultState(blockState.getBaseState().withProperty(StateList.directions, EnumFacing.NORTH));
		planeLocked = locked;
	}
	
	public BlockDirectional blacklistDirection(EnumFacing dir)
	{
		directionBlacklist.add(dir);
		return this;
	}
	
	public void printInfo(EntityPlayer ep) {}
	
	public BlockDirectional useAltPlacement()
	{
		useAlternatePlacementMethod = true;
		return this;
	}
	
	public IBlockState getStateForPlacement(World worldIn, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer)
    {
		EnumFacing direction;
		if (useAlternatePlacementMethod)
		{
	        direction = facing.getOpposite();
		}
		else
		{
			direction = (planeLocked ? FacMathHelper.getDirectionFromEntityXZ(pos, placer) : FacMathHelper.getDirectionFromEntity(pos, placer));
			if (!placer.isSneaking()) direction = direction.getOpposite();
		}
		if (directionBlacklist.contains(direction))
        {
        	for (EnumFacing f : EnumFacing.VALUES)
        	{
        		if (!directionBlacklist.contains(f))
        		{
        			direction = f;
        			break;
        		}
        	}
        }
		return getDefaultState().withProperty(StateList.directions, direction);
    }
	
	@Nonnull
	@Override
	public BlockStateContainer createBlockState()
	{
		return new BlockStateContainer(this, StateList.directions, PART_ID);
	}
	
	@Override
	public int getMetaFromState(IBlockState state)
	{
		return state.getValue(StateList.directions).ordinal();
	}
	
	@Override
	public IBlockState getStateFromMeta(int meta)
	{
		return blockState.getBaseState().withProperty(StateList.directions, EnumFacing.getFront(meta)).withProperty(PART_ID, 0);
	}

	@Override
	public void onWrenched(EntityPlayer ep, boolean isSneaking, World world, BlockPos pos, EnumFacing side) 
	{
		EnumFacing newFacing = world.getBlockState(pos).getValue(StateList.directions);
		do {
			if (planeLocked) newFacing = newFacing.rotateY();
			else newFacing = FacBlockHelper.nextRotation(newFacing, false);
		} while (directionBlacklist.contains(newFacing));
		world.setBlockState(pos, world.getBlockState(pos).withProperty(StateList.directions, newFacing));
	}
}