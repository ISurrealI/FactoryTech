package dalapo.factech.block;

import dalapo.factech.FactoryTech;
import dalapo.factech.helper.FacTileHelper;
import dalapo.factech.reference.AABBList;
import dalapo.factech.tileentity.automation.TileEntityItemRedis;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;

public class BlockItemRedis extends BlockTENoDir 
{

	public BlockItemRedis(Material materialIn, String name) {
		super(materialIn, name);
	}

	@Override
	public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer ep, EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ)
	{
		super.onBlockActivated(world, pos, state, ep, hand, side, hitX, hitY, hitZ);
		ep.openGui(FactoryTech.instance, 4, world, pos.getX(), pos.getY(), pos.getZ());
		return true;
	}
	
	@Override
	public AxisAlignedBB getCollisionBoundingBox(IBlockState state, IBlockAccess world, BlockPos pos)
	{
		return AABBList.TOP_IN;
	}
	
	@Override
	public void onEntityCollidedWithBlock(World world, BlockPos pos, IBlockState state, Entity entity)
	{
		if (entity instanceof EntityItem && !world.isRemote && entity.collidedVertically && (int)(entity.posY + 0.1) > pos.getY())
		{
			ItemStack is = ((EntityItem)entity).getItem();
			IItemHandler input = world.getTileEntity(pos).getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, EnumFacing.UP);
			is = FacTileHelper.tryInsertItem(input, is, EnumFacing.UP.getIndex());
			if (is.isEmpty()) entity.setDead();
		}
	}
}