package dalapo.factech.helper;

import dalapo.factech.reference.NameList;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.Vec3d;

public class FacEntityHelper
{
	private FacEntityHelper() {}
	
	public static void stopEntity(Entity entity)
	{
		entity.motionX = 0;
		entity.motionY = 0;
		entity.motionZ = 0;
	}

	public static void addMotion(Entity entity, Vec3d vec)
	{
		entity.motionX += vec.x;
		entity.motionY += vec.y;
		entity.motionZ += vec.z;
	}
	
	public static boolean isHoldingItem(EntityPlayer ep, Item item)
	{
		return ep.getHeldItemMainhand().getItem() == item || ep.getHeldItemOffhand().getItem() == item;
	}
	
	// Implementation taken from OpenBlocks
	public static NBTTagCompound getPersistentTag(EntityPlayer ep)
	{
		NBTTagCompound nbt = ep.getEntityData();
		NBTTagCompound persistTag = null;
		if (nbt.hasKey(EntityPlayer.PERSISTED_NBT_TAG))
		{
			persistTag = nbt.getCompoundTag(EntityPlayer.PERSISTED_NBT_TAG);
		}
		else
		{
			persistTag = new NBTTagCompound();
			nbt.setTag(EntityPlayer.PERSISTED_NBT_TAG, persistTag);
		}
		
		NBTTagCompound modTag = null;
		if (persistTag.hasKey(NameList.MODID))
		{
			modTag = persistTag.getCompoundTag(NameList.MODID);
		}
		else
		{
			modTag = new NBTTagCompound();
			persistTag.setTag(NameList.MODID, modTag);
		}
		return modTag;
	}
}