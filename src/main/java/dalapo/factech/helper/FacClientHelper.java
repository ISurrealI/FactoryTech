package dalapo.factech.helper;

import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;

@SideOnly(Side.CLIENT)
public class FacClientHelper
{
	public static void playSoundForClient(SoundEvent sound, SoundCategory category, float volume, float pitch)
	{
		World world = Minecraft.getMinecraft().world;
		EntityPlayer ep = Minecraft.getMinecraft().player;
		world.playSound(ep, ep.posX, ep.posY, ep.posZ, sound, category, volume, pitch);
	}
}