package dalapo.factech.gui;

import java.util.ArrayList;
import java.util.List;

import dalapo.factech.gui.widget.FacTechWidget;
import net.minecraft.client.gui.GuiScreen;

public class GuiFacScreen extends GuiScreen
{
	protected List<FacTechWidget> widgets = new ArrayList<FacTechWidget>();
	protected String texName;
	
	public GuiFacScreen(String tex)
	{
		texName = tex;
	}
	
	public void initGui()
	{
		super.initGui();
		for (FacTechWidget widget : widgets)
		{
			widget.init();
		}
	}
}