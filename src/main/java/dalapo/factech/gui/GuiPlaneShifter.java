package dalapo.factech.gui;

import dalapo.factech.auxiliary.PosWithDimension;
import dalapo.factech.gui.widget.WidgetFieldGauge;
import dalapo.factech.tileentity.TileEntityBasicInventory;
import dalapo.factech.tileentity.automation.TileEntityPlaneShifter;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.inventory.IInventory;

public class GuiPlaneShifter extends GuiBasicInventory
{
	public GuiPlaneShifter(ContainerBasicInventory container, IInventory player, TileEntityBasicInventory te)
	{
		super(container, player, "planeshifter_gui", te);
		addWidget(new WidgetFieldGauge(this, 30, 20, 17, 43, 0, "Fruit stored: "));
	}
	
	@Override
	public void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
	{
		super.drawGuiContainerForegroundLayer(mouseX, mouseY);
		PosWithDimension dim = ((TileEntityPlaneShifter)getTile()).getDimPos();
		if (dim != null)
		{
			int x = dim.getX();
			int y = dim.getY();
			int z = dim.getZ();
			int d = dim.getDimension();
			fontRenderer.drawString(String.format("(%s, %s, %s) in dimension %s", x, y, z, d), 20, 100, 0);
		}
	}
}