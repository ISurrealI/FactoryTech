package dalapo.factech.gui;

import dalapo.factech.tileentity.TileEntityBase;
import dalapo.factech.tileentity.automation.TileEntityItemRedis;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

public class ContainerItemRedis extends ContainerBase
{
	private static final EnumFacing[] PROPER_ORDER = {EnumFacing.UP, EnumFacing.DOWN, EnumFacing.NORTH, EnumFacing.SOUTH, EnumFacing.WEST, EnumFacing.EAST};
	public ContainerItemRedis(TileEntityItemRedis te, InventoryPlayer playerInv)
	{
		super(te);
		
		int slot = 0;
		for (int i=0; i<6; i++)
		{
			EnumFacing f = PROPER_ORDER[i];
			IItemHandler side = te.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, f);
			for (int j=0; j<5; j++)
			{
				addSlotToContainer(new SlotItemHandler(side, j, 35 + (i * 18), 61 + (j * 18)));
			}
		}
		
		if (playerInv != null)
		{
			for (int y = 0; y < 3; ++y)
			{
		        for (int x = 0; x < 9; ++x, slot++)
		        {
		            this.addSlotToContainer(new Slot(playerInv, x + y * 9 + 9, 8 + x * 18, 155 + y * 18));
		        }
		    }
			
			// Player hotbar
			for (int i=0; i<9; i++)
			{
				this.addSlotToContainer(new Slot(playerInv, i, 8 + i * 18, 213));
			}
		}
	}

	@Override
	public ItemStack transferStackInSlot(EntityPlayer playerIn, int index)
    {
		return ItemStack.EMPTY;
    }
	
	@Override
	public boolean canInteractWith(EntityPlayer playerIn)
	{
		return true;
	}
}