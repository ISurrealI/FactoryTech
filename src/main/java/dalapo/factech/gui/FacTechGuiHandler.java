package dalapo.factech.gui;

import java.util.ArrayList;
import java.util.List;

import dalapo.factech.gui.handbook.GuiHandbook;
import dalapo.factech.gui.widget.WidgetFieldGauge;
import dalapo.factech.gui.widget.WidgetNumberEntry;
import dalapo.factech.gui.widget.WidgetToggleSwitch;
import dalapo.factech.helper.MachineContainerFactory;
import dalapo.factech.helper.Pair;
import dalapo.factech.tileentity.TileEntityBasicInventory;
import dalapo.factech.tileentity.TileEntityFluidMachine;
import dalapo.factech.tileentity.TileEntityMachine;
import dalapo.factech.tileentity.TileEntityRFGenerator;
import dalapo.factech.tileentity.automation.TileEntityBufferCrate;
import dalapo.factech.tileentity.automation.TileEntityCrate;
import dalapo.factech.tileentity.automation.TileEntityFilterMover;
import dalapo.factech.tileentity.automation.TileEntityInventorySensor;
import dalapo.factech.tileentity.automation.TileEntityItemPusher;
import dalapo.factech.tileentity.automation.TileEntityItemRedis;
import dalapo.factech.tileentity.automation.TileEntityPipeValve;
import dalapo.factech.tileentity.automation.TileEntityPlaneShifter;
import dalapo.factech.tileentity.automation.TileEntityPulseCounter;
import dalapo.factech.tileentity.automation.TileEntityPulser;
import dalapo.factech.tileentity.automation.TileEntitySequencePlacer;
import dalapo.factech.tileentity.specialized.TileEntityAutoCrafter;
import dalapo.factech.tileentity.specialized.TileEntityCircuitScribe;
import dalapo.factech.tileentity.specialized.TileEntityCrucible;
import dalapo.factech.tileentity.specialized.TileEntityEnergizer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Slot;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class FacTechGuiHandler implements IGuiHandler {

//	private static final int AUTO_CRAFTER_X_OFFSET = 0;
//	private static final int AUTO_CRAFTER_Y_OFFSET = 0;
//	private static final Pair[] AUTO_CRAFTER_INPUTS = new Pair[9];
//	private static final Pair<Integer, Integer> AUTO_CRAFTER_OUTPUT = new Pair<>(100, 100);
//	
//	static {
//		for (int i=0; i<9; i++)
//		{
//			AUTO_CRAFTER_INPUTS[i] = new Pair<Integer, Integer>(AUTO_CRAFTER_X_OFFSET + 18*(i%3), AUTO_CRAFTER_Y_OFFSET + 18*(i/3));
//		}
//	}
	
	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		if (ID == 0)
		{
			TileEntityMachine te = (TileEntityMachine)world.getTileEntity(new BlockPos(x, y, z));
			return MachineContainerFactory.getContainer(te, player.inventory, te.getName());
//			return new ContainerBasicMachine(te.countPartSlots(), te, player.inventory, 35, 35, 89, 35);
		}
		else if (ID == 1 || ID == 12 || ID == 13)
		{
			TileEntityBasicInventory te = (TileEntityBasicInventory)world.getTileEntity(new BlockPos(x, y, z));
			return new ContainerBasicInventory(3, 3, te, player.inventory);
		}
		else if (ID == 2)
		{
			TileEntityAutoCrafter te = (TileEntityAutoCrafter)world.getTileEntity(new BlockPos(x, y, z));
			return new ContainerAutoCrafter(te, player);
		}
		else if (ID == 3)
		{
			TileEntityCrucible te = (TileEntityCrucible)world.getTileEntity(new BlockPos(x, y, z));
			return new ContainerCrucible(te, player.inventory);
		}
		else if (ID == 4)
		{
			TileEntityItemRedis te = (TileEntityItemRedis)world.getTileEntity(new BlockPos(x, y, z));
			return new ContainerItemRedis(te, player.inventory);
		}
		else if (ID == 5)
		{
			TileEntityPulser te = (TileEntityPulser)world.getTileEntity(new BlockPos(x, y, z));
			return new ContainerEmpty(te);
		}
		else if (ID == 7)
		{
			TileEntityCrate te = (TileEntityCrate)world.getTileEntity(new BlockPos(x, y, z));
			return new ContainerBasicInventory(2, 9, te, player.inventory);
		}
		else if (ID == 8)
		{
			TileEntityBasicInventory te = (TileEntityBasicInventory)world.getTileEntity(new BlockPos(x, y, z));
			return new ContainerBasicInventory(1, 1, te, player.inventory);
		}
		else if (ID == 9)
		{
			TileEntitySequencePlacer te = (TileEntitySequencePlacer)world.getTileEntity(new BlockPos(x, y, z));
			return new ContainerBasicInventory(2, 9, te, player.inventory);
		}
		else if (ID == 10)
		{
			TileEntityBufferCrate te = (TileEntityBufferCrate)world.getTileEntity(new BlockPos(x, y, z));
			return new ContainerBasicInventory(2, 9, te, player.inventory);
		}
		else if (ID == 11)
		{
			TileEntityInventorySensor te = (TileEntityInventorySensor)world.getTileEntity(new BlockPos(x, y, z));
			return new ContainerBasicInventory(3, 3, te, player.inventory);
		}
		else if (ID == 14)
		{
			TileEntityPulseCounter te = (TileEntityPulseCounter)world.getTileEntity(new BlockPos(x, y, z));
			return new ContainerEmpty(te);
		}
		else if (ID == 15)
		{
			TileEntityPlaneShifter te = (TileEntityPlaneShifter)world.getTileEntity(new BlockPos(x, y, z));
			List<Pair<Integer, Integer>> extraSlots = new ArrayList<>();
			extraSlots.add(new Pair<>(138, 35));
			return new ContainerBasicInventory(3, 3, extraSlots, te, player.inventory);
		}
		else if (ID == 16)
		{
			TileEntityRFGenerator te = (TileEntityRFGenerator)world.getTileEntity(new BlockPos(x, y, z));
			List<Pair<Integer, Integer>> list = new ArrayList<>();
			list.add(new Pair<>(152, 53));
			ContainerBasicInventory container = new ContainerBasicInventory(1, 1, list, te, player.inventory);
//			gui.addWidget(new WidgetEnergyGauge(gui, 120, 30, 20, 49));
			return container;
		}
		else if (ID == 17)
		{
			TileEntityPipeValve te = (TileEntityPipeValve)world.getTileEntity(new BlockPos(x, y, z));
			return new ContainerEmpty(te);
		}
		return null;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		if (ID == 0) // Machine
		{
			TileEntityMachine te = (TileEntityMachine)world.getTileEntity(new BlockPos(x, y, z));
			if (te != null) return MachineContainerFactory.getGui(te, player.inventory, te.getName());
//			return new GuiBasicMachine(new ContainerBasicMachine(te.countPartSlots(), te, player.inventory, 35, 35, 89, 35), player.inventory, te);
		}
		else if (ID == 1) // 3x3 inventory
		{
			TileEntityBasicInventory te = (TileEntityBasicInventory)world.getTileEntity(new BlockPos(x, y, z));
			return new GuiBasicInventory(new ContainerBasicInventory(3, 3, te, player.inventory), player.inventory, "stackfilter", te);
		}
		else if (ID == 2) // Autocrafter
		{
			TileEntityAutoCrafter te = (TileEntityAutoCrafter)world.getTileEntity(new BlockPos(x, y, z));
			return new GuiBasicMachine(new ContainerAutoCrafter(te, player), player.inventory, "autocrafter", te) {
				protected void drawProgressBar() {}
			};
		}
		else if (ID == 3) // 1 tank
		{
			TileEntityFluidMachine te = (TileEntityFluidMachine)world.getTileEntity(new BlockPos(x, y, z));
			return new GuiFluidMachine(new ContainerCrucible(te, player.inventory), player.inventory, te);
		}
		else if (ID == 4) // Item redis
		{
			TileEntityItemRedis te = (TileEntityItemRedis)world.getTileEntity(new BlockPos(x, y, z));
			return new GuiItemRedis(new ContainerItemRedis(te, player.inventory), player.inventory, "gui_item_redis", te);
		}
		else if (ID == 5) // RS Pulser
		{
			TileEntityPulser te = (TileEntityPulser)world.getTileEntity(new BlockPos(x, y, z));
			GuiBlank gui = new GuiBlank(te, new ContainerEmpty(te));
			gui.addWidget(new WidgetNumberEntry(gui, "pulser.numpulses", 0, 30, 60, 1, 32));
			gui.addWidget(new WidgetNumberEntry(gui, "pulser.ticksper", 1, 70, 60, 1, 20));
			gui.addWidget(new WidgetNumberEntry(gui, "pulser.ticksbetween", 2, 110, 60, 1, 20));
			return gui;
		}
		else if (ID == 6) // Handbook
		{
			return new GuiHandbook(player.getHeldItemMainhand());
		}
		else if (ID == 7) // Crate
		{
			TileEntityCrate te = (TileEntityCrate)world.getTileEntity(new BlockPos(x, y, z));
			return new GuiBasicInventory(new ContainerBasicInventory(2, 9, te, player.inventory), player.inventory, "crate_gui", te);
		}
		else if (ID == 8) // Single slot
		{
			TileEntityBasicInventory te = (TileEntityBasicInventory)world.getTileEntity(new BlockPos(x, y, z));
			return new GuiBasicInventory(new ContainerBasicInventory(1, 1, te, player.inventory), player.inventory, "energizer_gui", te);
		}
		else if (ID == 9) // Sequential placer. Could probably merge with crate tbh
		{
			TileEntitySequencePlacer te = (TileEntitySequencePlacer)world.getTileEntity(new BlockPos(x, y, z));
			return new GuiBasicInventory(new ContainerBasicInventory(2, 9, te, player.inventory), player.inventory, "sequencedropper", te);
		}
		else if (ID == 10) // Also crate...?
		{
			TileEntityBufferCrate te = (TileEntityBufferCrate)world.getTileEntity(new BlockPos(x, y, z));
			return new GuiBasicInventory(new ContainerBasicInventory(2, 9, te, player.inventory), player.inventory, "crate_gui", te);
		}
		else if (ID == 11) // Inventory sensor
		{
			TileEntityInventorySensor te = (TileEntityInventorySensor)world.getTileEntity(new BlockPos(x, y, z));
			return new GuiInventorySensor(new ContainerBasicInventory(3, 3, te, player.inventory), player.inventory, "stackfilter", te);
		}
		else if (ID == 12) // Filtered stack mover
		{
			TileEntityFilterMover te = (TileEntityFilterMover)world.getTileEntity(new BlockPos(x, y, z));
			GuiBasicInventory inv = new GuiBasicInventory(new ContainerBasicInventory(3, 3, te, player.inventory), player.inventory, "stackfilter", te);
			inv.addWidget(new WidgetToggleSwitch(inv, 0, 136, 20, "filtermover.roundrobin", "filtermover.firstmatch"));
			return inv;
		}
		else if (ID == 13) // Pulse piston
		{
			TileEntityItemPusher te = (TileEntityItemPusher)world.getTileEntity(new BlockPos(x, y, z));
			return new GuiPulsePiston(new ContainerBasicInventory(3, 3, te, player.inventory), player.inventory, "stackfilter", te);
		}
		else if (ID == 14) // RS Counter
		{
			TileEntityPulseCounter te = (TileEntityPulseCounter)world.getTileEntity(new BlockPos(x, y, z));
			GuiBlank gui = new GuiBlank(te, new ContainerEmpty(te));
			gui.addWidget(new WidgetNumberEntry(gui, "pulser.numpulses", 0, 70, 60, 1, 64));
			return gui;
		}
		else if (ID == 15)
		{
			TileEntityPlaneShifter te = (TileEntityPlaneShifter)world.getTileEntity(new BlockPos(x, y, z));
			List<Pair<Integer, Integer>> extraSlots = new ArrayList<>();
			extraSlots.add(new Pair<>(138, 35));
			ContainerBasicInventory container = new ContainerBasicInventory(3, 3, extraSlots, te, player.inventory);
			GuiPlaneShifter gui = new GuiPlaneShifter(container, player.inventory, te);
			return gui;
		}
		else if (ID == 16)
		{
			TileEntityRFGenerator te = (TileEntityRFGenerator)world.getTileEntity(new BlockPos(x, y, z));
			List<Pair<Integer, Integer>> list = new ArrayList<>();
			list.add(new Pair<>(152, 53));
			GuiBasicInventory gui = new GuiBasicInventory(new ContainerBasicInventory(1, 1, list, te, player.inventory), player.inventory, "generator_gui", te);
//			gui.addWidget(new WidgetEnergyGauge(gui, 120, 30, 20, 49));
			return gui;
		}
		else if (ID == 17)
		{
			TileEntityPipeValve te = (TileEntityPipeValve)world.getTileEntity(new BlockPos(x, y, z));
			return new GuiFluidValve(te, new ContainerEmpty(te));
		}
		return null;
	}
}