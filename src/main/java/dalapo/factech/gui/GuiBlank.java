package dalapo.factech.gui;

import java.util.ArrayList;
import java.util.List;

import dalapo.factech.gui.widget.FacTechWidget;
import dalapo.factech.gui.widget.WidgetNumberEntry;
import dalapo.factech.helper.FacGuiHelper;
import dalapo.factech.tileentity.TileEntityBase;
import dalapo.factech.tileentity.TileEntityBasicInventory;
import dalapo.factech.tileentity.automation.TileEntityPulser;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.inventory.Container;
import net.minecraft.util.ResourceLocation;

public class GuiBlank extends GuiFacInventory
{
	private TileEntityBase tile;
	public GuiBlank(TileEntityBase te, ContainerEmpty container)
	{
		super(container);
		tile = te;
	}
	
	@Override
	public void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
	{
		this.mc.getTextureManager().bindTexture(new ResourceLocation(FacGuiHelper.formatTexName("gui_fullblank")));
		this.drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);
		super.drawGuiContainerBackgroundLayer(partialTicks, mouseX, mouseY);
	}

	@Override
	public TileEntityBase getTile()
	{
		return tile;
	}
}