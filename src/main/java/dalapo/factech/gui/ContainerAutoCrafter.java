package dalapo.factech.gui;

import dalapo.factech.helper.FacMiscHelper;
import dalapo.factech.helper.Pair;
import dalapo.factech.tileentity.specialized.TileEntityAutoCrafter;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ClickType;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.world.World;

public class ContainerAutoCrafter extends ContainerBasicMachine {
	
	private static final int AUTO_CRAFTER_X_OFFSET = 62;
	private static final int AUTO_CRAFTER_Y_OFFSET = 17;
	private static final Pair[] AUTO_CRAFTER_INPUTS = new Pair[9];
	private static final Pair<Integer, Integer> AUTO_CRAFTER_OUTPUT = new Pair<>(152, 35);
	
	static {
		for (int i=0; i<9; i++)
		{
			AUTO_CRAFTER_INPUTS[i] = new Pair<Integer, Integer>(AUTO_CRAFTER_X_OFFSET + 18*(i%3), AUTO_CRAFTER_Y_OFFSET + 18*(i/3));
		}
	}
	
	private TileEntityAutoCrafter te;
	private World worldObj;
	private InventoryCrafting matrix;
	
	public ContainerAutoCrafter(TileEntityAutoCrafter te, EntityPlayer ep)
	{
		super(1, 0, te, ep.inventory, AUTO_CRAFTER_INPUTS, new Pair[] {AUTO_CRAFTER_OUTPUT});
		this.worldObj = te.getWorld(); // Should be safe since the constructor is called server-side
		this.matrix = new InventoryCrafting(this, 3, 3);
		te.updateValues(this.matrix);
	}
	
	 
	@Override
	public ItemStack slotClick(int slotId, int dragType, ClickType clicktype, EntityPlayer ep)
	{
//		Logger.info(te.getStackInSlot(slotId));
		return super.slotClick(slotId, dragType, clicktype, ep);
	}
	@Override
	public void onCraftMatrixChanged(IInventory handler)
	{
		super.onCraftMatrixChanged(handler);
		// More stuff if necessary
	}
	
	public ContainerAutoCrafter(TileEntityAutoCrafter te, World world)
	{
		this(te, FacMiscHelper.getFakePlayer(world));
	}
	
	@Override
	public boolean canInteractWith(EntityPlayer playerIn) {
		return true;
	}
	
	@Override
	public void onContainerClosed(EntityPlayer playerIn)
    {
        InventoryCrafting mtrx = new InventoryCrafting(this, 3, 3);
        IRecipe result = CraftingManager.findMatchingRecipe(mtrx, worldObj);
        if (result != null && !result.getRecipeOutput().isEmpty())
        {
        	te.updateValues(mtrx);
        }
        super.onContainerClosed(playerIn);
    }
}