package dalapo.factech.gui.widget;

import dalapo.factech.gui.GuiTileEntity;
import dalapo.factech.helper.FacGuiHelper;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;

public class WidgetFieldGauge extends FacTechWidget
{
	private String label;
	private int field;
	public WidgetFieldGauge(GuiTileEntity parent, int x, int y, int w, int h, int field, String label)
	{
		super(parent, x, y, w, h);
		this.label = label;
		this.field = field;
	}

	@Override
	public void init() 
	{
		// no-op
	}

	@Override
	public void handle(int mouseX, int mouseY, int mouseButton, boolean shift)
	{
		// no-op
	}

	@Override
	public String getTooltip()
	{
		return label + parent.getTile().getVal(field);
	}

	@Override
	public void draw(int guiLeft, int guiTop)
	{
		FacGuiHelper.bindTex("widgets");
		drawTexturedModalRect(x + guiLeft, y + guiTop, 0, 49, 18, 45);
		int height = (int)(this.height * parent.getTile().getVal(field) / parent.getTile().getMaxVal(field));
		this.drawRect(x+guiLeft+1, y+guiTop+(this.height-height+1), x+guiLeft+this.width, y+guiTop+this.height+1, 0x80FF0000);
	}
}