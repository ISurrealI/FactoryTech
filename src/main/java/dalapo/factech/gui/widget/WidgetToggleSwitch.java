package dalapo.factech.gui.widget;

import dalapo.factech.gui.GuiFacInventory;
import dalapo.factech.gui.GuiTileEntity;
import dalapo.factech.helper.FacGuiHelper;
import dalapo.factech.helper.Logger;
import dalapo.factech.packet.PacketHandler;
import dalapo.factech.packet.SwitchTogglePacket;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.ResourceLocation;

public class WidgetToggleSwitch extends FacTechWidget
{
	private boolean state;
	private int id;
	private String onMessage;
	private String offMessage;
	
	public WidgetToggleSwitch(GuiTileEntity parent, int id, int x, int y, String off, String on)
	{
		super(parent, x, y, 18, 10);
		this.id = id;
		onMessage = on;
		offMessage = off;
	}

	public void init()
	{
		setState(parent.getTile().getVal(id) > 0);
	}
	
	public void handle(int mouseX, int mouseY, int mouseButton, boolean shift)
	{
		state = !state;
		parent.getTile().toggleVal(id); // Client-side
		PacketHandler.sendToServer(new SwitchTogglePacket(parent.getTile().getPos(), (byte)id)); // Server-side
	}
	
	public int getID()
	{
		return id;
	}
	
	@Override
	public void draw(int parX, int parY)
	{
		GlStateManager.pushMatrix();
		FacGuiHelper.bindTex("widgets");
		GlStateManager.color(1F, 1F, 1F, 1F);
		this.drawTexturedModalRect(x + parX, y + parY, 0, state ? 10 : 0, width, height);
		GlStateManager.popMatrix();
	}
	
	public void setState(boolean val)
	{
		state = val;
	}
	
	@Override
	public String getTooltip()
	{
		return state ? onMessage : offMessage;
	}
}