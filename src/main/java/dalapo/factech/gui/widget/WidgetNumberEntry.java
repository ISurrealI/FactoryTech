package dalapo.factech.gui.widget;

import dalapo.factech.gui.GuiFacInventory;
import dalapo.factech.gui.GuiTileEntity;
import dalapo.factech.helper.FacGuiHelper;
import dalapo.factech.helper.Logger;
import dalapo.factech.packet.NumberEntryPacket;
import dalapo.factech.packet.PacketHandler;
import dalapo.factech.tileentity.TileEntityBase;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.init.SoundEvents;

public class WidgetNumberEntry extends FacTechWidget
{
	private int val;
	private final int min;
	private final int max;
	private byte fieldNum;
	private String tooltip;
	
	public WidgetNumberEntry(GuiTileEntity parent, String tooltip, int fieldNum, int x, int y, int min, int max) {
		super(parent, x, y, 18, 29);
		this.fieldNum = (byte)fieldNum;
		this.min = min;
		this.max = max;
		this.tooltip = tooltip;
	}

	@Override
	public void init()
	{
		val = parent.getTile().getVal(fieldNum);
	}

	private void changeVal(int dv)
	{
		if (val + dv > max) val = max;
		else if (val + dv < min) val = min;
		else val += dv;
	}
	
	@Override
	public void handle(int mouseX, int mouseY, int mouseButton, boolean shift)
	{
		int lowerUpper = y+8;
		int upperLower = y+height-8;
		int dv = shift ? 10 : 1;
		if (mouseY < lowerUpper)
		{
			changeVal(dv);
		}
		else if (mouseY > upperLower)
		{
			changeVal(-dv);
		}
		getParent().getTile().setVal(fieldNum, val); // fine
		PacketHandler.sendToServer(new NumberEntryPacket(fieldNum, val));
		Minecraft.getMinecraft().getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundEvents.UI_BUTTON_CLICK, 1.0F));
	}

	@Override
	public String getTooltip()
	{
		return tooltip;
	}

	@Override
	public void draw(int parX, int parY)
	{
		GlStateManager.pushMatrix();
		FacGuiHelper.bindTex("widgets");
		GlStateManager.color(1F, 1F, 1F, 1F);
		drawTexturedModalRect(x + parX, y + parY, 0, 20, width, height);
//		zLevel++;
		Minecraft.getMinecraft().fontRenderer.drawString(Integer.toString(val), 4 + x + parX, 10 + y + parY, 0x303030);
		GlStateManager.popMatrix();
	}
}
